FROM centos:7

RUN yum install -y epel-release && yum install -y uwsgi uwsgi-plugin-python3 

RUN mkdir /app
RUN mkdir /app/src

CMD ["uwsgi", "/app/uwsgi.ini"]
