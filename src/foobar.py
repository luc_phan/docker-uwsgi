import sys

def application(env, start_response):
    start_response('200 OK', [('Content-Type','text/html')])
    content = "Python version: {}".format(sys.version)
    return [content.encode('utf-8')]
